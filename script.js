const container = document.getElementById ('container');
const keyboard = document.getElementsByClassName('keyboard');
const text = document.getElementById('text');

 let word = prompt ('Introduceți cuvântul:');

let constructedWord=[];

for (var i=0;i<word.length;i++) {
    if(i<word.length-1){
    constructedWord[i]='_ | ';}
    else {
        constructedWord[i]='_'; 
    }
};

let temp=[];

const guessedLetters = document.createElement('p');
guessedLetters.innerText=`Ghiciți cuvântul: ${constructedWord.join('')} `;
guessedLetters.setAttribute('id','guessedLetters');
text.appendChild (guessedLetters);

let noOfGuesses= 10; //^the number of lines on the hangman
let currentImg= document.createElement('img');
currentImg.src='/img/hangman0.png';
container.appendChild(currentImg);  


let button=document.getElementById('button');
let input=document.getElementById('input');
let finalWord=[];



function onClick(button) {
    button.addEventListener('click', ()=>{
        currentGuess=button.innerText.toLowerCase();
        if (finalWord.join('')!=word && noOfGuesses!=0) {
            let isItThere=false;
            let noOfEncounters=0;   
            
            
            for (var i=0; i<word.length;i++) { 
                if (word[i] == currentGuess) {
                    if (i<word.length) {
                    constructedWord[i]=word[i] + ' | ';
                    }
                    else {
                        constructedWord[i]=word[i]; 
                    }
                    finalWord[i]=word[i];
                    isItThere=true;
                    noOfEncounters++;
                    guessedLetters.innerText=`Ghiciți cuvantul: ${constructedWord.join('')}`;
                    
                }
                
        
                }; 
            if (isItThere) {
                button.disabled = true;
                button.removeAttribute('id','button');
                button.setAttribute('id',"buttonAfterClickRight");
                if (noOfEncounters == 1) {
                    console.log(`${currentGuess} se găsește în cuvânt o singură dată` );
                }
                else {
                    console.log(`${currentGuess} se găsește în cuvânt de ${noOfEncounters} ori`);
                }
            }
            else {
                button.disabled = true;
                button.removeAttribute('id','button');
                button.setAttribute('id',"buttonAfterClickWrong")
                noOfGuesses--;
                console.log(`Numărul de încercări rămase este: ${noOfGuesses}`);
        
                currentImg.src='/img/hangman'+(10-noOfGuesses)+'.png';
                container.appendChild(currentImg);            
        };

        if (finalWord.join('')==word) {
            
            guessedLetters.innerText=`Felicitări! Ați ghicit cuvântul: ${finalWord.join('')}`;
            console.log(`Felicitări! Ați ghicit cuvântul: ${finalWord.join('')}`);
            
        
        }
            else if (noOfGuesses==0 && finalWord.join('')!==word) {
        
                console.log(`Ne pare rău! Nu ați ghicit cuvântul!`);
                guessedLetters.innerText=`Ne pare rău! Nu ați ghicit cuvântul!\n Soluția era: ${word}`;
                
                
            }
        }
        
        

        
        
})};




let alphabet='QWERTYUIOPASDFGHJKLZXCVBNM';
let alphabetButton=[];

let firstRow=document.getElementById('firstRow');
let secondRow=document.getElementById('secondRow');
let thirdRow=document.getElementById('thirdRow');

for  (var i=0; i<alphabet.length; i++) {
    alphabetButton[i] = document.createElement('button');
    alphabetButton[i].innerText=alphabet[i];
    i<10 ? firstRow.appendChild(alphabetButton[i]) :
    i<19 ? secondRow.appendChild(alphabetButton[i]):
    thirdRow.appendChild(alphabetButton[i]);
    alphabetButton[i].setAttribute('id','button');
    onClick(alphabetButton[i]);
    
    
}










